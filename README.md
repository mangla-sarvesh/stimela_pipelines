# STIMELA Pipelines

# gmrt_selfcal_gaincal_wsclean.py

Do 5 time of self-calibration on multiple visibilites simultaneously using STIMELA.

**First install stimela and create and environment to work in it.**

Create the environment as:

(i) go the stimela directory

```
cd stimela_env/venv3/
```

(ii) create the environment

```
source bin/activate
```

**Now go to your directory containing data and do as below the following steps:**

Create 3 new directory

```
mkdir input output msdir
```

put all the .ms files in msdir directory
**Edit the "gmrt_selfcal_gaincal_wsclean.py" according to your requirement**

keep in mind (the last parameters for ncycles==5 are for aplitude selfcal)

**Then run this file in ipython as:**

```
#execfile('/home/usr_name/stimela_pipelines/gmrt_selfcal_gaincal_wsclean.py')
ipython -i /home/usr_name/stimela_pipelines/gmrt_selfcal_gaincal_wsclean.py
```

**Check the final selfcal image. You can again make a mask on this file and create an image using this one. It should give you a good image.**

# gmrt_selfcal_cubical_wsclean.py

_Still under development_

Contact: mangla.sarvesh@gmail.com

