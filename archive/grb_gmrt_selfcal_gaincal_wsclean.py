import stimela
import glob, os
from pathlib import Path

INPUT="input"
MSDIR="msdir"
OUTPUT="output"

homepath = os.path.expanduser('~')
pipepath = os.path.join(homepath,'git/stimela_pipelines/')

working_dir = os.path.join(os.getcwd(),"msdir")
msfilenames = sorted(glob.glob(os.path.join(working_dir,'*.ms')))

mslist = []
for file in range(len(msfilenames)):
    mslist.append(Path(msfilenames[file]).name)

#mslist = ["data_1.ms","data_2.ms","data_3.ms"]
#mslist = ['COSMOSc_4k_16s.ms']

#########################################################
# Initial imaging Parameters

image_size = 5120
pixel_size = "0.5asec"

#########################################################
# SelfCal Parameters

ref_ant_1 = "5,4,1,26"
ref_ant_2 = "1,5,4,25"
ref_ant_3 = "2,25,4,5"

flagging = input('Flagging Residiual (y/n) >>> ')


ref_ant = [ref_ant_1,ref_ant_2,ref_ant_3]
uvrange = ''

ncycles = 5                              ## the number of phase cal cycles
soltime = ['6min','5min','4min','3min','2min']    ## solution time interval
thresh = [50e-6,40e-6,30e-6,20e-6,10e-6]         ## clean threshold
sigma = [8,7.5,7,6.5,6]                        ## The sigma clipping for generating CLEAN mask
iteration = [20000,30000,30000,40000,50000]  # CLEAN threshold

#########################################################
#########################################################
# First blind Image and create a mask

recipe_blind = stimela.Recipe("First blind image",ms_dir=MSDIR,JOB_TYPE="docker")

recipe_blind.add("cab/wsclean", "First image", {
     "msname": mslist,
     "datacolumn": "DATA",
     "multiscale":True,
     "join-channels": True,
     "channels-out": 2,
     "fit-spectral-pol":2,
     "size": [image_size, image_size],
     "scale": pixel_size,
     "mgain": 0.8,
#     "local-rms": True,
#     "local-rms-window":15, 
     "weighting-rank-filter":3,
     "weighting-rank-filter-size": 32, 
#     "fitsmask":'CDFS_S01-MFS-image.mask1.fits:input',
#     "threshold":20.e-6,
     "auto-mask":7.0,
     "auto-threshold":0.5, 
     "weight":'briggs 1.0',
     "niter": 10000,
#     "no-update-model-required":True,
     "name": "target_1st_blind",
     
}, input=INPUT, output=OUTPUT, label="first blind")
 
recipe_blind.run()


os.system('python3 '+ os.path.join(pipepath,"make_mask.py")+ ' output/target_1st_blind-MFS-image.fits --threshold=6')

#########################################################
#########################################################
# First Masked image to create a better mask

recipe_masked = stimela.Recipe("First masked image",ms_dir=MSDIR,JOB_TYPE="docker")

recipe_masked.add("cab/wsclean", "First image with mask", {
     "msname": mslist,
     "datacolumn": "DATA",
     "multiscale":True,
     "join-channels": True,
     "channels-out": 2,
     "fit-spectral-pol":2,
     "size": [image_size, image_size],
     "scale": pixel_size,
     "mgain": 0.8,
#     "local-rms": True,
#     "local-rms-window":15, 
     "weighting-rank-filter":3,
     "weighting-rank-filter-size": 32, 
     "fitsmask":'target_1st_blind-MFS-image.mask.fits:output',
#     "threshold":20.e-6,
#     "auto-mask":8.0,
     "auto-threshold":4.0, 
     "weight":'briggs 1.0',
     "niter": 15000,
#     "no-update-model-required":True,
     "name": "target_1st_masked",
     
}, input=INPUT, output=OUTPUT, label="first masked")
 
recipe_masked.run()


os.system('python3 '+ os.path.join(pipepath,"make_mask.py")+ ' output/target_1st_masked-MFS-image.fits --threshold=6 --outfile output/target_mask_0.fits')

#########################################################
#########################################################
# ncycle times Phase selfcal

recipe_selfcal = stimela.Recipe("Selfcal",ms_dir=MSDIR,JOB_TYPE="docker")

for jj in range(ncycles):
    for ii in range(len(mslist)):
        if jj<4:
            recipe_selfcal.add("cab/casa_gaincal", f"calibrating {ii} ms {jj} cycle",{
                "msname": mslist[ii],
                "caltable":'gain_'+str(ii)+'th_ms.'+str(jj),
                "field":'0',
                "refant":ref_ant[ii],
                "gaintype":'G',
                "refantmode":'flex',
                "solint":soltime[jj],
                "minsnr":2,
                "minblperant": 4,
                "uvrange":uvrange,
                "calmode":'p'
                }, input=INPUT, output=OUTPUT, label=f"cal {ii} ms {jj} cycle")
        if jj==4:
            recipe_selfcal.add("cab/casa_gaincal", f"calibrating {ii} ms {jj} cycle",{
                "msname": mslist[ii],
                "caltable":'gain_'+str(ii)+'th_ms.'+str(jj),
                "field":'0',
                "refant":ref_ant[ii],
                "gaintype":'G',
                "refantmode":'flex',
                "solint":soltime[jj],
                "minsnr":2.5,
                "minblperant": 4,
                "uvrange":uvrange,
                "calmode":'ap' 
                }, input=INPUT, output=OUTPUT, label=f"cal {ii} ms {jj} cycle")
        recipe_selfcal.add("cab/msutils", f"plot_gains {ii} ms {jj} cycle", {
            "command": "plot_gains",
            "ctable": 'gain_'+str(ii)+'th_ms.'+str(jj)+':output',
            "tabtype": "gain",
            "plot_file": 'gains_'+str(ii)+'th_ms_'+str(jj)+'.png',
            "subplot_scale": 4,
            "plot_dpi": 180,
            }, input=INPUT, output=OUTPUT, label=f"plot gains {ii} ms {jj} cycle")
        recipe_selfcal.add("cab/casa_applycal", f"applying {ii} ms {jj} cycle",{
            "msname": mslist[ii],
            "gaintable":['gain_'+str(ii)+'th_ms.'+str(jj)+':output'],
            "applymode":'calflagstrict',
            "calwt":False,
            }, input=INPUT, output=OUTPUT, label=f"apply to {ii} ms {jj} cycle")
        if flagging=='y':
            recipe_selfcal.add("cab/casa_flagdata", f"flagging residual amp cal loop {ii} ms {jj} cycle",{
                "msname": mslist[ii],
                "mode":'tfcrop',
                "timecutoff":7.0,
                "freqcutoff":7.0,
                "usewindowstats":'both',
                "halfwin":2,
                "datacolumn": "RESIDUAL",
                "extendflags":False, 
                }, input=INPUT, output=OUTPUT, label=f"flag amp cal loop {ii} ms {jj} cycle")
        recipe_selfcal.add('cab/msutils', f"Copy corr column to data of {ii} ms {jj} cycle", {
            "command" : 'copycol',
            "msname"  : mslist[ii],
            "fromcol" : 'CORRECTED_DATA',
            "tocol"   : 'DATA',
            }, input=INPUT, output=OUTPUT, label=f"copy {ii} ms {jj} cycle")
    recipe_selfcal.add("cab/wsclean", f"Image after selfcal {ii} ms {jj} cycle", {
        "msname": mslist,
        "datacolumn": "CORRECTED_DATA",
        "multiscale":True,
        "join-channels": True,
        "channels-out": 2,
        "fit-spectral-pol":2,
        "size": [image_size, image_size],
        "scale": pixel_size,
        "mgain": 0.8,
        "weighting-rank-filter":3,
        "weighting-rank-filter-size": 32, 
        "fitsmask":'target_mask_'+str(jj)+'.fits:output',
        "threshold":thresh[jj],
        "weight":'briggs 1',
        "niter": iteration[jj],
        "name": "target_masked_"+str(jj),
        }, input=INPUT, output=OUTPUT, label=f"mask image {ii} ms {jj} cycle")
    recipe_selfcal.add("cab/cleanmask", f"mask {ii} ms {jj} cycle", {
        "image":"target_masked_"+str(jj)+'-MFS-image.fits:output',
        "output": 'target_mask_'+str(jj+1)+'.fits',
        "sigma":sigma[jj],
        "boxes": 50,
        "tolerance":0.15,
        "no-negative":False,
        "overlap":0,
        }, input=INPUT, output=OUTPUT, label=f"mask creation {ii} ms {jj} cycle")

recipe_selfcal.run()

#########################################################
#########################################################
